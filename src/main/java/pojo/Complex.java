package pojo;

public class Complex {
    String name;
    String value;
    User user;

    public Complex(String name, String value, User user) {
        this.name = name;
        this.value = value;
        this.user = user;
    }

    public Complex() {

    }

    public String toString() {
        return "Complex{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", user=" + user +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
