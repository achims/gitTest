package JSONTest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import pojo.Complex;
import pojo.User;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 对json进行复习
 */
public class JavaToJson {

    ObjectMapper mapper = new ObjectMapper();

    /**
     * 将对象转换成json类型,对象需要有无参构造
     */
    @Test
    public void projectToJson() {
        User user = new User("张三", "123456");
        try {
            String str = mapper.writeValueAsString(user);
            System.out.println("对应的数据为" + str);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    /**
     * 目的；为了将对应的复杂类型装载,同时将对应的数据装载进入json文件之中
     */
    @Test
    public void complexJSON() {
        User user = new User("张三", "11111");
        Complex complex = new Complex("李四", "宝宝", user);
        try {
            mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);//空值填充
            String value = mapper.writeValueAsString(complex);
            System.out.println("对应的数据为" + value);
            mapper.writeValue(new File("complexJSON.json"), complex);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 集合类型装载
     */
    @Test
    public void MapJSON() {
        Map<String, User> map = new HashMap<>();
        map.put("高级教师", new User("张三", "111"));
        try {
            String value = mapper.writeValueAsString(map);
            System.out.println(value);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    /**
     * list类型
     */
    @Test
    public void ListJSON() {
        LinkedList<Map<String, User>> list = new LinkedList<>();

        Map<String, User> map = new HashMap<>();
        map.put("高级教师", new User("张三", "111"));
        Map<String, User> map2 = new HashMap<>();
        map.put("中级教师", new User("李四", "111"));
        list.add(map);
        list.add(map2);
        try {
            String value = mapper.writeValueAsString(list);
            System.out.println(value);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }


}
