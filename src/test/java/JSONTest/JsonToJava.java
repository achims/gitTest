package JSONTest;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pojo.User;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 将目的：将对应的字符串转换成
 */
public class JsonToJava {

     ObjectMapper mapper=new ObjectMapper();

    public Logger logger = LoggerFactory.getLogger(JsonToJava.class);


    /**
     * 字符串转换为简单数据类型
     */
    @Test
    public void strToEasy() {
        String str = "{\"name\":\"张三\",\"password\":\"123456\"}";
        try {
            User user = mapper.readValue(str,User.class);
            System.out.println(user);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 将str转换成list对象
     */

    @Test
    public void strToList() {
        List<User>list=new ArrayList<>();
        list.add(new User("张三", "111"));
        list.add(new User("lis", "111"));
        try {
            mapper.writeValue(new File("list.json"),list);
        } catch (IOException e) {
            e.printStackTrace();
        }
        InputStream stream = JsonToJava.class.getResourceAsStream("/list.json");
        JavaType type = mapper.getTypeFactory().constructParametricType(List.class, User.class);
        try {
            List<User> users =(List<User>) mapper.readValue(stream, type);
            for(User user:users){
                System.out.println(user);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
